package top.todu.demo.spirngcloud.gateway.demo.config;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CancellationException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/** filter 名字为前缀 ，也就是 TcpService， GatewayFilterFactory 是固定后缀 */
@Slf4j
@Component
public class TcpService2GatewayFilterFactory
    extends AbstractGatewayFilterFactory<TcpService2GatewayFilterFactory.Config> implements Ordered {

  public TcpService2GatewayFilterFactory() {
    super(Config.class);
  }

  @Override
  public GatewayFilter apply(Config config) {
    return (exchange, chain) -> {
      log.info("自定义：2, isCommitted: {}", exchange.getResponse().isCommitted());
      Mono<Void> filter = chain.filter(exchange);
      log.info("自定义：2, isCommitted: {}", exchange.getResponse().isCommitted());
      return filter;
    };
  }

  @Override
  public int getOrder() {
    return 2;
  }

  public static class Config {
    private String host;
    private int port;

    public String getHost() {
      return host;
    }

    public void setHost(String host) {
      this.host = host;
    }

    public int getPort() {
      return port;
    }

    public void setPort(int port) {
      this.port = port;
    }
  }
}
